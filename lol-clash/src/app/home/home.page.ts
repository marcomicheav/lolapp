import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  public pageName = 'Home';
  public imageUrl = '../../assets/images/mochilaRappi.jpg'
  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  }

  changeItemImage(url: string){
    this.imageUrl = url;    
  }

}
